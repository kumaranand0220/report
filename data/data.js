var data=[
  true,
  [
    {
      "_id": "5894a36b9e50cb4a090bf50f",
      "clientName": "asdasdadad",
      "creator": {
        "login": "n.wnuk@wp.pl"
      },
      "modificator": {
        "login": "n.wnuk@wp.pl"
      },
      "createdDate": 1486136171377,
      "modificationDate": 1486136171377,
      "panelData": [
        {
          "id": 1,
          "title": "PRODUKTY / USLUGI",
          "nodes": [],
          "fields": [
            {
              "regId": "5881016f231fb902d4b8c14c",
              "nodeId": "1",
              "control_type": "Radio Group",
              "label": "Czy posiadasz produkt/usluge/wartosc  która odniosla sukces na rynku  polskim?",
              "description": "",
              "checker": false,
              "options": [
                "10(pelen sukces)",
                " 5(jestem na dobrej drodze)",
                " 1(jestem na poczatku drogi)"
              ],
              "id": 1,
              "reports": [],
              "value1": "",
              "value2": "",
              "value3": "",
              "value4": "",
              "selectedListItems": []
            },
            {
              "regId": "5881016f231fb902d4b8c14c",
              "nodeId": "1",
              "control_type": "Radio Group",
              "label": "Czy dokonales adaptacji produktu,  jego nazwy, opakowan i materialów  marketingowych do nowego rynku i  jego specyfiki?",
              "description": "",
              "options": [
                "10(tak wszystko gotowe)",
                " 5(jestem na dobrej drodze)",
                " 1(jeszcze nie)"
              ],
              "checker": false,
              "id": 2,
              "reports": [],
              "value1": "",
              "value2": "",
              "value3": "",
              "value4": "",
              "selectedListItems": []
            },
            {
              "regId": "5881016f231fb902d4b8c14c",
              "nodeId": "1",
              "control_type": "Textarea",
              "label": "Czy istnieje duza konkurencja dla  twojego produktu/uslugi?",
              "description": "",
              "checker": false,
              "id": 3,
              "reports": [],
              "value1": "",
              "value2": "",
              "value3": "",
              "value4": "",
              "selectedListItems": []
            },
            {
              "regId": "5881016f231fb902d4b8c14c",
              "nodeId": "1",
              "control_type": "Radio Group",
              "label": "Czy twój produkt posiada unikalna  wartosc sprzedazowa (unique selling  proposition), która jest skierowana na  nowy rynek?",
              "description": "(Unikatowa propozycja sprzedazy; czynnik,  który wyróznia dany produkt, sposród innych  w jego sektorze. USP jest argumentem, na  którego podstawie buduje sie przekaz  reklamowy. To unikalna korzysc, jaka klient  nabywa wraz z zakupem produktu/uslugi)",
              "options": [
                "Tak posiada",
                " Nie wiem czy ta która sie sprawdzala w Polsce sprawdzi sie na nowym rynku",
                " Nie posiada"
              ],
              "checker": false,
              "id": 5,
              "reports": [],
              "value1": "",
              "value2": "",
              "value3": "",
              "value4": "",
              "selectedListItems": []
            },
            {
              "regId": "5881016f231fb902d4b8c14c",
              "nodeId": "1",
              "control_type": "Checkbox Group",
              "label": "Jaka najwieksza wartosc generujemy  dla klienta na nowym rynku w stosunku  do istniejacej na nim konkurencji ?",
              "description": "Wybierz trzy najwazniejsze cechy",
              "checker": false,
              "sort": null,
              "options": [
                "cena",
                " szybkosc  obslugi",
                " doswiadczenie ",
                " dopasowanie do  klienta",
                " nowy zestaw  potrzeb o  których nie  wiedzieli",
                " skutecznosc –  odciaza klienta",
                " dostepnosc",
                " marka i status",
                " nizsze koszty –  obnizanie  kosztów  dzialalnosci  klientów",
                " nizsze ryzyko –  dluga gwarancja",
                "  wygoda i  uzytecznosc -  np. prostota"
              ],
              "id": 6,
              "reports": [],
              "value1": "",
              "value2": "",
              "value3": "",
              "value4": "",
              "selectedListItems": []
            },
            {
              "regId": "5881016f231fb902d4b8c14c",
              "nodeId": "1",
              "control_type": "Custom List",
              "label": "Rekomendacje",
              "description": "Wybierz z listy",
              "customList": {
                "listIdValue": 4
              },
              "id": 46,
              "reports": [],
              "value1": "",
              "value2": "",
              "value3": "",
              "value4": "",
              "selectedListItems": []
            }
          ]
        },
        {
          "id": 2,
          "title": "NOWE RYNKI / KLUCZOWI KLIENCI",
          "nodes": [],
          "fields": [
            {
              "regId": "5881016f231fb902d4b8c14c",
              "nodeId": "2",
              "control_type": "Radio Group",
              "label": "Czy wiesz który/które z rynków moze  byc zaineresowany/chlonny/podatny na  twój produkt/usluge?",
              "checker": false,
              "options": [
                "Wiem",
                " Wiem ale chetnie dokonam takiej analizy",
                " Nie wiem. Chce sie przekonac czy moje spostrzezenia sa trafne"
              ],
              "description": "",
              "id": 7,
              "reports": [],
              "value1": "",
              "value2": "",
              "value3": "",
              "value4": "",
              "selectedListItems": []
            },
            {
              "regId": "5881016f231fb902d4b8c14c",
              "nodeId": "2",
              "control_type": "Radio Group",
              "label": "Czy wiesz jakie sa luki na ryku? (Czy  wiesz jaka czesc rynku mozesz zajac?  Jaki jest potencjal rynkowy?)",
              "description": "",
              "options": [
                "Nie wiem",
                " Nie wiem ale postanowilem sie  przekonac",
                " Chetnie przeanalizuje jeszcze raz z  czym sie spotkam"
              ],
              "checker": false,
              "id": 8,
              "reports": [],
              "value1": "",
              "value2": "",
              "value3": "",
              "value4": "",
              "selectedListItems": []
            },
            {
              "regId": "5881016f231fb902d4b8c14c",
              "nodeId": "2",
              "control_type": "Radio Group",
              "label": "Czy na rynku docelowym platnosci  regulowane sa na biezaco?",
              "options": [
                "tak",
                " nie",
                " nie wiem"
              ],
              "checker": false,
              "id": 9,
              "reports": [],
              "value1": "",
              "value2": "",
              "value3": "",
              "value4": "",
              "selectedListItems": []
            },
            {
              "regId": "5881016f231fb902d4b8c14c",
              "nodeId": "2",
              "control_type": "Radio Group",
              "label": "Czy odwiedziles - interesujacy ciebie  rynek?",
              "options": [
                "Nie",
                " Zamierzam to zrobic",
                " Tak"
              ],
              "id": 10,
              "reports": [],
              "value1": "",
              "value2": "",
              "value3": "",
              "value4": "",
              "selectedListItems": []
            },
            {
              "regId": "5881016f231fb902d4b8c14c",
              "nodeId": "2",
              "control_type": "Slider",
              "label": "Czy wiesz jak duzo specyficznych dla  rynku barier jest do pokonania wedlug  twojej oceny aby na niego wejsc?",
              "slider": {
                "min": 1,
                "max": 10
              },
              "description": "1(malo), 10(bardzo duzo)",
              "checker": false,
              "id": 11,
              "reports": [],
              "value1": "",
              "value2": "",
              "value3": "",
              "value4": "",
              "selectedListItems": []
            },
            {
              "regId": "5881016f231fb902d4b8c14c",
              "nodeId": "2",
              "control_type": "Radio Group",
              "label": "Czy znasz wszystkie wymogi rynku aby  na nim zaistniec ?",
              "description": "np. wymóg  swiadczenia uslug serwisowych  gwarancyjnych i pogwarancyjnych,  zwroty towaru etc",
              "options": [
                "Znam",
                " Nie znam",
                " Musze dowiedziec sie wiecej"
              ],
              "checker": false,
              "id": 12,
              "reports": [],
              "value1": "",
              "value2": "",
              "value3": "",
              "value4": "",
              "selectedListItems": []
            },
            {
              "regId": "5881016f231fb902d4b8c14c",
              "nodeId": "2",
              "control_type": "Radio Group",
              "label": "Czy posiadasz niezbedne  ubezpieczenie chroniace Ciebie od  niepozadanych zdarzen na nowym  rynku?",
              "placeholder": "",
              "options": [
                "Posiadam",
                " Nie posiadam",
                " Nie ma takiej potrzeby"
              ],
              "checker": false,
              "id": 13,
              "reports": [],
              "value1": "",
              "value2": "",
              "value3": "",
              "value4": "",
              "selectedListItems": []
            },
            {
              "regId": "5881016f231fb902d4b8c14c",
              "nodeId": "2",
              "control_type": "Custom List",
              "label": "Rekomendacje",
              "description": "Wybierz z listy",
              "customList": {
                "listIdValue": 4
              },
              "id": 47,
              "reports": [],
              "value1": "",
              "value2": "",
              "value3": "",
              "value4": "",
              "selectedListItems": []
            }
          ]
        },
        {
          "id": 3,
          "title": "KANALY DOTARCIA DO RYNKÓW, KLIENTÓW",
          "nodes": [],
          "fields": [
            {
              "regId": "5881016f231fb902d4b8c14c",
              "nodeId": "3",
              "control_type": "Custom List",
              "label": "Na jakich kanalach chcialbys  sie skupic w poczatkowej fazie  eksportu?",
              "description": "Wybór z listy",
              "customList": {
                "listIdValue": 1
              },
              "checker": false,
              "id": 14,
              "reports": [],
              "value1": "",
              "value2": "",
              "value3": "",
              "value4": "",
              "selectedListItems": []
            },
            {
              "regId": "5881016f231fb902d4b8c14c",
              "nodeId": "3",
              "control_type": "Custom List",
              "label": "Jakie kanaly chcialbys miec docelowo?",
              "customList": {
                "listIdValue": 1
              },
              "checker": false,
              "id": 15,
              "reports": [],
              "value1": "",
              "value2": "",
              "value3": "",
              "value4": "",
              "selectedListItems": []
            },
            {
              "regId": "5881016f231fb902d4b8c14c",
              "nodeId": "3",
              "control_type": "Slider",
              "label": "Czy nasze kanaly sa zintegrowane?",
              "slider": {
                "min": 1,
                "max": 10
              },
              "description": "1(nie zintegrowane), 10(zintegrowane)",
              "checker": false,
              "id": 16,
              "reports": [],
              "value1": "",
              "value2": "",
              "value3": "",
              "value4": "",
              "selectedListItems": []
            },
            {
              "regId": "5881016f231fb902d4b8c14c",
              "nodeId": "3",
              "control_type": "Radio Group",
              "label": "Czy masz opracowane  standardowe zasady  postepowania z klientem ? Czy  dotyczy to równiez nowego  rynku?",
              "options": [
                "Tak",
                " Jestem w trakcie",
                " Nie",
                " Nie widze takiej potrzeby"
              ],
              "checker": false,
              "id": 17,
              "reports": [],
              "value1": "",
              "value2": "",
              "value3": "",
              "value4": "",
              "selectedListItems": []
            },
            {
              "regId": "5881016f231fb902d4b8c14c",
              "nodeId": "3",
              "control_type": "Radio Group",
              "label": "Czy wiesz jak dopasowac  kanaly dotarcia z  dotychczasowymi  przyzwyczajeniami klientów na  nowych rynkach?",
              "options": [
                "Tak",
                " Jestem w trakcie",
                " Nie",
                " Nie widze takiej potrzeby"
              ],
              "checker": false,
              "id": 18,
              "reports": [],
              "value1": "",
              "value2": "",
              "value3": "",
              "value4": "",
              "selectedListItems": []
            },
            {
              "regId": "5881016f231fb902d4b8c14c",
              "nodeId": "3",
              "control_type": "Radio Group",
              "label": "Czy wiesz jakie kanaly dotarcia  do klientów bede  najefektywniejsze/ czego  oczekuja od Ciebie twoi klienci?",
              "checker": false,
              "id": 19,
              "reports": [],
              "options": [
                "Tak",
                " Jestem w trakcie",
                " Nie",
                " Nie widze takiej potrzeby"
              ],
              "value1": "",
              "value2": "",
              "value3": "",
              "value4": "",
              "selectedListItems": []
            },
            {
              "regId": "5881016f231fb902d4b8c14c",
              "nodeId": "3",
              "control_type": "Custom List",
              "label": "Rekomendacje",
              "description": "Wybierz z listy",
              "customList": {
                "listIdValue": 4
              },
              "id": 48,
              "reports": [],
              "value1": "",
              "value2": "",
              "value3": "",
              "value4": "",
              "selectedListItems": []
            }
          ]
        },
        {
          "id": 4,
          "title": "RELACJE Z RYNKAMI / KLIENTAMI",
          "nodes": [],
          "fields": [
            {
              "regId": "5881016f231fb902d4b8c14c",
              "nodeId": "4",
              "control_type": "Slider",
              "label": "Czy masz juz jakies kontakty na  docelowym rynku?",
              "description": "1(nie mam kontaktów) , 10(tak szerokie kontakty)",
              "slider": {
                "min": 1,
                "max": 10
              },
              "checker": false,
              "id": 20,
              "reports": [],
              "value1": "",
              "value2": "",
              "value3": "",
              "value4": "",
              "selectedListItems": []
            },
            {
              "regId": "5881016f231fb902d4b8c14c",
              "nodeId": "4",
              "control_type": "Slider",
              "label": "Jak trudno zdobyc Ci nowe kontakty na  nowym rynku?",
              "description": "1(bardzo trudno), 10 (latwo)",
              "slider": {
                "min": 1,
                "max": 10
              },
              "checker": false,
              "id": 21,
              "reports": [],
              "value1": "",
              "value2": "",
              "value3": "",
              "value4": "",
              "selectedListItems": []
            },
            {
              "regId": "5881016f231fb902d4b8c14c",
              "nodeId": "4",
              "control_type": "Slider",
              "label": "W jakim stopniu pracownicy sa  przygotowani do utrzymywania relacji z  klientem na nowym/róznym kulturowo  rynku?",
              "slider": {
                "min": 1,
                "max": 10
              },
              "description": "1(bardzo slabo), 10(swietnie)",
              "checker": false,
              "id": 22,
              "reports": [],
              "value1": "",
              "value2": "",
              "value3": "",
              "value4": "",
              "selectedListItems": []
            },
            {
              "regId": "5881016f231fb902d4b8c14c",
              "nodeId": "4",
              "control_type": "Slider",
              "label": "Ocen w skali od 1 do 10 czy udalo ci sie  pokonac bariere jezykowa?",
              "description": "1(ogromna bariera), 10(nie mam zadnej bariery)",
              "slider": {
                "min": 1,
                "max": 10
              },
              "checker": false,
              "id": 23,
              "reports": [],
              "value1": "",
              "value2": "",
              "value3": "",
              "value4": "",
              "selectedListItems": []
            },
            {
              "regId": "5881016f231fb902d4b8c14c",
              "nodeId": "4",
              "control_type": "Slider",
              "label": "Czy wiesz jakiego rodzaju relacji oczekuja  od Ciebie klienci na nowym rynku?",
              "description": "1(nie wiem), 10(wiem doskonale)",
              "slider": {
                "min": 1,
                "max": 10
              },
              "checker": false,
              "id": 24,
              "reports": [],
              "value1": "",
              "value2": "",
              "value3": "",
              "value4": "",
              "selectedListItems": []
            },
            {
              "regId": "5881016f231fb902d4b8c14c",
              "nodeId": "4",
              "control_type": "Slider",
              "label": "Ilu tym oczekiwaniom udalo ci sie  sprostac?",
              "description": "1(zadnemu), 10(wszystkim)",
              "checker": false,
              "slider": {
                "min": 1,
                "max": 10
              },
              "id": 25,
              "reports": [],
              "value1": "",
              "value2": "",
              "value3": "",
              "value4": "",
              "selectedListItems": []
            },
            {
              "regId": "5881016f231fb902d4b8c14c",
              "nodeId": "4",
              "control_type": "Radio Group",
              "label": "Na ile róznie sie w relacjach z klientem od  konkurencji na nowym rynku?",
              "options": [
                "jestem lepszy/bardziej sie staram",
                " jestem taki sam",
                " mnóstwo pracy przede mna"
              ],
              "checker": false,
              "id": 26,
              "reports": [],
              "value1": "",
              "value2": "",
              "value3": "",
              "value4": "",
              "selectedListItems": []
            },
            {
              "regId": "5881016f231fb902d4b8c14c",
              "nodeId": "4",
              "control_type": "Radio Group",
              "label": "Czy planowales juz swoja obecnosc  internetowa na nowym rynku?",
              "options": [
                "Nie. na tym etapie nie jest mi potrzebna",
                " Rozgladam sie za istniejacymi  rozwiazaniami",
                " Tak podjalem takie dzialania"
              ],
              "sort": null,
              "checker": false,
              "id": 27,
              "reports": [],
              "value1": "",
              "value2": "",
              "value3": "",
              "value4": "",
              "selectedListItems": []
            },
            {
              "regId": "5881016f231fb902d4b8c14c",
              "nodeId": "4",
              "control_type": "Custom List",
              "description": "Wybierz z listy",
              "label": "Rekomendacje",
              "customList": {
                "listIdValue": 4
              },
              "id": 49,
              "reports": [],
              "value1": "",
              "value2": "",
              "value3": "",
              "value4": "",
              "selectedListItems": []
            }
          ]
        },
        {
          "id": 5,
          "title": "KLUCZOWI PARTNERZY",
          "nodes": [],
          "fields": [
            {
              "regId": "5881016f231fb902d4b8c14c",
              "nodeId": "5",
              "control_type": "Radio Group",
              "label": "Czy widzisz potrzebe zwiazania sie/  podjales juz wspólprace z partnerem  prawnym odnosnie pokonywania  ograniczen prawnych i proceduralnych  zwiazanych z nowym rynkiem?",
              "options": [
                "Tak",
                " Nie wiem",
                " Nie"
              ],
              "checker": false,
              "id": 28,
              "reports": [],
              "value1": "",
              "value2": "",
              "value3": "",
              "value4": "",
              "selectedListItems": []
            },
            {
              "regId": "5881016f231fb902d4b8c14c",
              "nodeId": "5",
              "control_type": "Radio Group",
              "label": "Czy myslales aby podjac wspólprace z i  polskim lub innym przedsiebiorca aby  wejsc na nowy dla partnerów rynek ?",
              "checker": false,
              "options": [
                "Tak",
                " Nie wiem",
                " Nie"
              ],
              "id": 29,
              "reports": [],
              "value1": "",
              "value2": "",
              "value3": "",
              "value4": "",
              "selectedListItems": []
            },
            {
              "regId": "5881016f231fb902d4b8c14c",
              "nodeId": "5",
              "control_type": "Checkbox Group",
              "label": "W jakim celu bylbys ewentualnie sklonny  podjac taka wspólprace?",
              "description": "Zaznacz opcje",
              "options": [
                "optymalizacja korzysci i skali",
                " obnizanie poziomu ryzyka i niepewnosci",
                " wspóldzielenie konkretnych zasobów lub dzialan"
              ],
              "checker": false,
              "id": 30,
              "reports": [],
              "value1": "",
              "value2": "",
              "value3": "",
              "value4": "",
              "selectedListItems": []
            },
            {
              "regId": "5881016f231fb902d4b8c14c",
              "nodeId": "5",
              "control_type": "Textarea",
              "label": "Jakich innych strategicznych partnerów  chcialbys pozyskac do wspólpracy?",
              "description": "3pozycje np. ubezpieczenia  Transport etc",
              "checker": false,
              "id": 31,
              "reports": [],
              "value1": "",
              "value2": "",
              "value3": "",
              "value4": "",
              "selectedListItems": []
            },
            {
              "regId": "5881016f231fb902d4b8c14c",
              "nodeId": "5",
              "control_type": "Switch",
              "label": "Czy wiesz jak ich znalezc ?",
              "switch": {
                "true": "Tak",
                "false": "Nie, przydalaby sie pomoc"
              },
              "checker": false,
              "id": 32,
              "reports": [],
              "value1": "",
              "value2": "",
              "value3": "",
              "value4": "",
              "selectedListItems": []
            },
            {
              "regId": "5881016f231fb902d4b8c14c",
              "nodeId": "5",
              "control_type": "Custom List",
              "label": "Rekomendacje",
              "description": "Wybierz z listy",
              "customList": {
                "listIdValue": 4
              },
              "id": 50,
              "reports": [],
              "value1": "",
              "value2": "",
              "value3": "",
              "value4": "",
              "selectedListItems": []
            }
          ]
        },
        {
          "id": 6,
          "title": "STRUMIENIE PRZYCHODÓW",
          "nodes": [],
          "fields": [
            {
              "regId": "5881016f231fb902d4b8c14c",
              "nodeId": "6",
              "control_type": "Radio Group",
              "label": "Czy wiesz ile sa w stanie zaplacic klienci  na nowym rynku za twój produkt/usluge?",
              "options": [
                "Wiem",
                " Chetnie to przeanalizuje jeszcze raz",
                " Nie wiem"
              ],
              "checker": false,
              "id": 33,
              "reports": [],
              "value1": "",
              "value2": "",
              "value3": "",
              "value4": "",
              "selectedListItems": []
            },
            {
              "regId": "5881016f231fb902d4b8c14c",
              "nodeId": "6",
              "control_type": "Radio Group",
              "label": "Czy wiesz jakie mechanizmy cenowe sa  obecne / najbardziej  rozpowszechnione/akceptowane na nowym  rynku?",
              "options": [
                "Wiem",
                " Chetnie to przeanalizuje jeszcze raz",
                " Nie wiem"
              ],
              "checker": false,
              "id": 34,
              "reports": [],
              "value1": "",
              "value2": "",
              "value3": "",
              "value4": "",
              "selectedListItems": []
            },
            {
              "regId": "5881016f231fb902d4b8c14c",
              "nodeId": "6",
              "control_type": "Custom List",
              "label": "Czy wiesz jakie mechanizmy cenowe  bedziesz stosowal na nowym rynku?",
              "customList": {
                "listIdValue": 2
              },
              "checker": false,
              "id": 35,
              "reports": [],
              "value1": "",
              "value2": "",
              "value3": "",
              "value4": "",
              "selectedListItems": []
            },
            {
              "regId": "5881016f231fb902d4b8c14c",
              "nodeId": "6",
              "control_type": "Radio Group",
              "label": "Czy wiesz w jaki sposób sa dokonywane  platnosci? Jakich sposobów/mozliwosci  oczekuje rynek?",
              "options": [
                "Wiem",
                " Chetnie to przeanalizuje jeszcze raz",
                " Nie wiem"
              ],
              "checker": false,
              "id": 36,
              "reports": [],
              "value1": "",
              "value2": "",
              "value3": "",
              "value4": "",
              "selectedListItems": []
            },
            {
              "regId": "5881016f231fb902d4b8c14c",
              "nodeId": "6",
              "control_type": "Slider",
              "label": "Ocen w skali od 1 do 10 swoja wiedze na  temat platnosci pomiedzy rynkami.",
              "slider": {
                "min": 1,
                "max": 10
              },
              "description": "1(bardzo slaba), 10(swietna)",
              "id": 37,
              "reports": [],
              "value1": "",
              "value2": "",
              "value3": "",
              "value4": "",
              "selectedListItems": []
            },
            {
              "regId": "5881016f231fb902d4b8c14c",
              "nodeId": "6",
              "control_type": "Radio Group",
              "label": "Czy wiesz jak sie dodatkowo zabezpieczyc  sie przed ewentualnymi klopotami z  platnosciami?",
              "options": [
                "Wiem",
                " Chetnie to przeanalizuje jeszcze raz",
                " Nie wiem"
              ],
              "checker": false,
              "id": 38,
              "reports": [],
              "value1": "",
              "value2": "",
              "value3": "",
              "value4": "",
              "selectedListItems": []
            },
            {
              "regId": "5881016f231fb902d4b8c14c",
              "nodeId": "6",
              "control_type": "Switch",
              "label": "Czy przychody sa przewidywalne",
              "switch": {
                "true": "Tak",
                "false": "Nie"
              },
              "checker": false,
              "id": 39,
              "reports": [],
              "value1": "",
              "value2": "",
              "value3": "",
              "value4": "",
              "selectedListItems": []
            },
            {
              "regId": "5881016f231fb902d4b8c14c",
              "nodeId": "6",
              "control_type": "Switch",
              "label": "Czy obecnie Twoje przychody sa zdywersyfikowane?",
              "switch": {
                "true": "Tak",
                "false": "Nie"
              },
              "checker": false,
              "id": 40,
              "reports": [],
              "value1": "",
              "value2": "",
              "value3": "",
              "value4": "",
              "selectedListItems": []
            },
            {
              "regId": "5881016f231fb902d4b8c14c",
              "nodeId": "6",
              "control_type": "Custom List",
              "description": "Wybierz z listy",
              "label": "Rekomendacje",
              "customList": {
                "listIdValue": 4
              },
              "id": 51,
              "reports": [],
              "value1": "",
              "value2": "",
              "value3": "",
              "value4": "",
              "selectedListItems": []
            }
          ]
        },
        {
          "id": 7,
          "title": "STRUKTURA KOSZTÓW",
          "nodes": [],
          "fields": [
            {
              "regId": "5881016f231fb902d4b8c14c",
              "nodeId": "7",
              "control_type": "Slider",
              "label": "Czy masz zarezerwowane  srodki/wystarczajace srodki w budzecie do  przeprowadzenia dzialan  proeksportowych?",
              "description": "1(Nie mam), 10(mam duzo)",
              "slider": {
                "min": 1,
                "max": 10
              },
              "checker": false,
              "id": 41,
              "reports": [],
              "value1": "",
              "value2": "",
              "value3": "",
              "value4": "",
              "selectedListItems": []
            },
            {
              "regId": "5881016f231fb902d4b8c14c",
              "nodeId": "7",
              "control_type": "Switch",
              "label": "Czy dokonywales symulacji kosztów  niezbednych do przygotowania  sie/rozpoczecia/rozszerzenia eksportu",
              "switch": {
                "true": "Tak",
                "false": "Nie"
              },
              "checker": false,
              "id": 42,
              "reports": [],
              "value1": "",
              "value2": "",
              "value3": "",
              "value4": "",
              "selectedListItems": []
            },
            {
              "regId": "5881016f231fb902d4b8c14c",
              "nodeId": "7",
              "control_type": "Checkbox Group",
              "label": "Które kluczowe dzialania eksportowe  wymagaja od Ciebie najwiekszych  nakladów finansowych?",
              "checker": false,
              "options": [
                "tworzenie oprogramowania",
                "  projektowanie towarów/uslug",
                "  wytwarzanie towarów/uslug",
                "  dostarczanie do klienta",
                "  ciagla promocja"
              ],
              "id": 43,
              "reports": [],
              "value1": "",
              "value2": "",
              "value3": "",
              "value4": "",
              "selectedListItems": []
            },
            {
              "regId": "5881016f231fb902d4b8c14c",
              "nodeId": "7",
              "control_type": "Slider",
              "label": "Czy bariera w finansowaniu kosztów  dzialan jest problem z uzyskaniem kredytu?",
              "sort": null,
              "slider": {
                "min": 1,
                "max": 14
              },
              "checker": false,
              "id": 44,
              "reports": [],
              "value1": "",
              "value2": "",
              "value3": "",
              "value4": "",
              "selectedListItems": []
            },
            {
              "regId": "5881016f231fb902d4b8c14c",
              "nodeId": "7",
              "control_type": "Slider",
              "label": "Jak dalece wazne/decydujace jest  otrzymanie dotacji na dzialania  proeksportowe?",
              "slider": {
                "min": 1,
                "max": 10
              },
              "checker": false,
              "id": 45,
              "reports": [],
              "value1": "",
              "value2": "",
              "value3": "",
              "value4": "",
              "selectedListItems": []
            },
            {
              "regId": "5881016f231fb902d4b8c14c",
              "nodeId": "7",
              "control_type": "Custom List",
              "label": "Rekomendacje",
              "description": "Wybierz z listy",
              "customList": {
                "listIdValue": 4
              },
              "id": 52,
              "reports": [],
              "value1": "",
              "value2": "",
              "value3": "",
              "value4": "",
              "selectedListItems": []
            }
          ]
        }
      ],
      "version": 0,
      "superNodesData": [],
      "regId": "588b90dff9c2d213e376795a"
    }
  ]
];