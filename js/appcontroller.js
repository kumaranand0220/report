angular.module('recursionDemo').controller("TreeController", function($scope,$http) {
	//$scope.treeDir = data;
	
	var transforms = {
		'dirNode':{
			'<>':'li',
			'children':[
				{"<>":"span","html":" Node"},
				{
					'tag':'table',
					'html':[
						{'tag':'tr','html':[
							{'tag':'td','class':'name','html':'Id  '},
							{'tag':'td','class':'type','html':'${id}'}
						]},
						{'tag':'tr','html':[
							{'tag':'td','class':'name','html':'Title  '},
							{'tag':'td','class':'type','html':'${title}'}
						]}
					]
				}
			],
			'html':function(obj){
				return(json2html.transform(obj, transforms.node));
			}
		},
		"node": {
        			"<>":"ul",
        			'html':function(obj){
						return(json2html.transform(obj.fields, transforms.fields));
					},
					'children':function(obj){
						return(json2html.transform(obj.nodes, transforms.dirNode));
					}
		},
		'fields':{
			'<>':'li',
			'html':[
                {"<>":"span","html":" fields"},
                {
                	"<>":"table",
					'children':function(obj){
						var arr = [];
						$.each(obj, function(key, val){
							var type = $.type(val);
							if(type != 'object' && type != 'array' ){
								var d = {};
								d['name'] = key;
								d['value'] = val;
								arr.push(d);
							}
						});
						
						return(json2html.transform(arr, transforms.field));
					}
            	}
            ]
		},
		'field':{
			'<>':'tr',
			'children':[
				{'tag':'td','html':'${name}'},
				{'tag':'td','html':'${value}'}
			]
		}
	};[{'name':'sdfsdf'}]
	console.log(data[1][0].panelData);
    $('#json').json2html(data[1][0].panelData,transforms.dirNode);
 //    $('a.dir').click(function(e){
	// 	$(this).next().toggleClass('show');
	// 	return false;
	// });
})