
angular.module('recursionDemo').directive("tree", function(RecursionControl) {
    return {
        restrict: "E",
        scope: {dir: '='},
        template: 
            '<table style="border-collapse: collapse; margin-left:10px; min-width:200px; width:100%">' + 
                '<caption style="background-color:bisque; margin-top:5px; padding-left:10px;"><p style="text-align:left;">{{ dir.title }} / {{ dir.id }}</p></caption>'+
                '<tr ng-repeat="subDir in dir.nodes">' + 
                    '<td><tree dir="subDir"></tree></td>' +
                '</tr>' + 
                '<tr ng-if="dir.fields" ng-repeat="subDir in dir.fields">' + 
                    '<td style="padding-left:10px; border-top:1px dotted #e5e5e5; border-left:1px dotted #e5e5e5">'+
                    '<h3>{{ subDir.name }}</h3>'+ 
                    '<p>{{ subDir.value }}</p>'+
                    '</td>'+
                '</tr>' +
            '</table>',
        compile: function(element) {
            return RecursionControl.compile(element, function(scope, iElement, iAttrs, controller, transcludeFn){
            	
            });
        }
    };
});